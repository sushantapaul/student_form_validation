<?php include('./pertials/header.php'); ?>
<?php include('./pertials/navber.php'); ?>
<?php
    include('connect.php');
    session_start();

    $id = $_GET['id'];

    if(empty($id))
    {
        $_SESSION['msz'] = "Violent use";
        header('Location: ./index.php');
    }
    else
    {
        $sql = "SELECT * FROM `person` WHERE id=$id";
        $data = $conn->query($sql);
        $item = $data->fetch_assoc();
        if(empty($item))
        {
            $_SESSION['msz'] = "Invalid Id";
            header('Location: ./index.php');
        }
    }

?>

    <section>
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-4 offset-md-4">
                    <table class="table table-bordered">
                        <caption class="text-danger text-center py-3">Person's Information</caption>
                        <tr>
                            <th>Name:</th>
                            <td><?php echo $item['name']; ?></td>
                        </tr>
                        <tr>
                            <th>Email:</th>
                            <td><?php echo $item['email']; ?></td>
                        </tr>
                        <tr>
                            <th>Address:</th>
                            <td><?php echo $item['address']; ?></td>
                        </tr>
                        <tr>
                            <th>Mobile Number:</th>
                            <td><?php echo $item['mobile']; ?></td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </section>


<?php include('./pertials/footer.php'); ?>