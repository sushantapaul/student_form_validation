<?php include('./pertials/header.php'); ?>
<?php include('./pertials/navber.php'); ?>
<?php session_start(); ?>

<section>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-4 offset-md-4">
                <h2 class="text-center text-danger py-3">Create Account</h2>
                <div>
                    <?php 
                        if(isset($_SESSION['msz'])) 
                        { ?>
                            <p class="text-danger py-2 alert alert-danger"><?php echo '<i class="fas fa-exclamation-circle px-2"></i>'.$_SESSION['msz']; ?></p>
                            <?php  session_destroy();
                        }
                    ?>
                </div>
                <form action="insert.php" method="POST">
                    <div class="form-group">
                        <input type="text" name="name" class="form-control" id="" aria-describedby="emailHelp" placeholder="Your name">
                    </div>
                    <div class="form-group">
                        <input type="email" name="email" class="form-control" id="" aria-describedby="emailHelp" placeholder="email">
                    </div>
                    <div class="form-group">
                        <input type="text" name="address" class="form-control" id="" aria-describedby="emailHelp" placeholder="address">
                    </div>
                    <div class="form-group">
                        <input type="tel" name="mobile" class="form-control" id="" aria-describedby="emailHelp" placeholder="mobile">
                    </div>
                    <div class="form-group">
                        <button type="submit" name="submit" class="form-control btn btn-success">submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>

<?php include('./pertials/footer.php'); ?>