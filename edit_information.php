<?php include('./pertials/header.php'); ?>
<?php include('./pertials/navber.php'); ?>

<?php
    include('connect.php');
    session_start();

    $id = $_GET['id'];

    if(empty($id))
    {
        $_SESSION['msz2'] = "Id is required";
        header('Location: ./index.php');
    }
    else
    {
        $sql = "SELECT * FROM `person` WHERE id=$id";
        $data = $conn->query($sql);
        $item = $data->fetch_assoc();
        if(empty($item))
        {
            $_SESSION['msz2'] = "Invalid Id";
            header('Location: ./index.php');
        }
    }

?>

<section>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-4 offset-md-4">
                <h2 class="text-center text-danger py-3">Edit your account</h2>
                <div>
                    <?php
                        if(isset($_SESSION['msz']))
                        { ?>
                            <p class="text-danger py-2 alert alert-danger"><?php echo '<i class="fas fa-exclamation-circle px-2"></i>'.$_SESSION['msz']; ?></p>
                            <?php  session_destroy();
                        }
                    ?>
                </div>
                <form action="update.php" method="POST">
                    <div class="form-group d-none">
                        <input type="text" name="id" class="form-control" id="" aria-describedby="emailHelp" value="<?php echo $item['id']; ?>">
                    </div>
                    <div class="form-group">
                        <input type="text" name="name" class="form-control" id="" aria-describedby="emailHelp" value="<?php echo $item['name']; ?>">
                    </div>
                    <div class="form-group">
                        <input type="email" name="email" class="form-control" id="" aria-describedby="emailHelp" value="<?php echo $item['email']; ?>">
                    </div>
                    <div class="form-group">
                        <input type="text" name="address" class="form-control" id="" aria-describedby="emailHelp" value="<?php echo $item['address']; ?>">
                    </div>
                    <div class="form-group">
                        <input type="tel" name="mobile" class="form-control" id="" aria-describedby="emailHelp" value="<?php echo $item['mobile']; ?>">
                    </div>
                    <div class="form-group">
                        <button type="submit" name="update" class="form-control btn btn-success">update</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>


<?php include('./pertials/footer.php'); ?>