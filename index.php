<?php include('./pertials/header.php'); ?>
<?php include('./pertials/navber.php'); ?>
<?php
  include('connect.php');
  session_start();

  $sql = "SELECT * FROM `person`";

  $data = $conn->query($sql);

?>

<section>
  <div class="container-fluid">
    <div class="row row-cols-1">
      <div class="col-md-8 offset-md-2">
        <div class="table-responsive">
          <div>
              <?php
                  if(isset($_SESSION['msz']))
                  { ?>
                    <p class="text-danger py-2 alert alert-danger"><?php echo '<i class="fas fa-exclamation-circle px-2"></i>'.$_SESSION['msz'];?></p>
                    <?php session_unset();
                  }
                  elseif(isset($_SESSION['msz2']))
                  { ?>
                    <p class="text-danger py-2 alert alert-danger"><?php echo '<i class="fas fa-exclamation-circle px-2"></i>'.$_SESSION['msz2'];?></p>
                    <?php session_unset();
                  }
                  elseif(isset($_SESSION['success']))
                  { ?>
                    <p class="text-success py-2 alert alert-success"><?php echo '<i class="fas fa-check px-2"></i>'.$_SESSION['success'];?></p>
                    <?php session_unset();
                  }
              ?>
          </div>
          <table class="table table-bordered">
            <caption>List of User</caption>
            <thead>
              <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Email</th>
                <th>Address</th>
                <th>Mobile</th>
                <th>Action</th>
              </tr>
            </thead>

            <?php foreach($data as $item) { ?>
              <tbody>
                <tr>
                  <td><?php echo $item['id']; ?></td>
                  <td><?php echo $item['name']; ?></td>
                  <td><?php echo $item['email']; ?></td>
                  <td><?php echo $item['address']; ?></td>
                  <td><?php echo $item['mobile']; ?></td>
                  <td class="text-center">
                    <a href="./view_profile.php?id=<?php echo $item['id']; ?>" class="text-success px-1"><i class="fas fa-street-view"></i></a>
                    <a href="./edit_information.php?id=<?php echo $item['id']; ?>" class="text-info px-1"><i class="far fa-edit"></i></a>
                    <form action="./delete.php" method="POST" class="d-inline">
                      <input type="text" class="d-none" name="id" value="<?php echo $item['id'];?>">
                      <button type="submit" name="delete" class="bg-white border-0 text-danger"><i class="fas fa-trash-alt"></i></button>
                    </form>
                  </td>
                </tr>
              </tbody>
            <?php } ?>
          </table>
        </div>
      </div>
    </div>
  </div>
</section>





<?php include('./pertials/footer.php'); ?>

